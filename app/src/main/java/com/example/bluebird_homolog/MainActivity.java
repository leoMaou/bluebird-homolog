package com.example.bluebird_homolog;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private Button btnLoad;
    private Button btnStatus;
    private Button btnConectar;
    private Button btnDesconectar;
    private Button btnIniciarLeitura;
    private Button btnPararLeitura;

    private Button btnSetTxPower;
    private Button btnGetTxPower;
    private Button btnSetSession;
    private Button btnGetSession;
    private Button btnSetTarget;
    private Button btnGetTarget;
    private Button btnSetTxCicleOn;
    private Button btnGetTxCicleOn;
    private Button btnSetTxCicleOff;
    private Button btnGetTxCicleOff;
    private Button btnSetParamInventory;
    private Button btnGetParamInventory;

    private EditText etTxCicleOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLoad = (Button) findViewById(R.id.btnLoad);
        btnStatus = (Button) findViewById(R.id.btnStatus);
        btnConectar = (Button) findViewById(R.id.btnConectar);
        btnDesconectar = (Button) findViewById(R.id.btnDesconectar);
        btnIniciarLeitura = (Button) findViewById(R.id.btnIniciarLeitura);
        btnPararLeitura = (Button) findViewById(R.id.btnPararLeitura);

        btnSetTxPower = (Button) findViewById(R.id.btnSetTxPower);
        btnGetTxPower = (Button) findViewById(R.id.btnGetTxPower);
        btnSetSession = (Button) findViewById(R.id.btnSetSession);
        btnGetSession = (Button) findViewById(R.id.btnGetSession);
        btnSetTarget = (Button) findViewById(R.id.btnSetTarget);
        btnGetTarget = (Button) findViewById(R.id.btnGetTarget);
        btnSetTxCicleOn = (Button) findViewById(R.id.btnSetTxCicleOn);
        btnGetTxCicleOn = (Button) findViewById(R.id.btnGetTxCicleOn);
        btnSetTxCicleOff = (Button) findViewById(R.id.btnSetTxCicleOff);
        btnGetTxCicleOff = (Button) findViewById(R.id.btnGetTxCicleOff);
        btnSetParamInventory = (Button) findViewById(R.id.btnSetParamInventory);
        btnGetParamInventory = (Button) findViewById(R.id.btnGetParamInventory);

        btnSetTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Setando target");
                BlueBirdController blueBirdController = new BlueBirdController();
                EditText etTarget = (EditText) findViewById(R.id.etTarget);
                String target = etTarget.getText().toString();

                try {
                    JSONObject jo = new JSONObject();
                    jo.put("target", target);
                    JSONArray ja = new JSONArray();
                    ja.put(jo);

                    blueBirdController.execute("setTarget", ja, getApplicationContext());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Target to set = " + target);
            }
        });

        btnGetTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Pegando target");

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("getTarget", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao carregar conexão");
                    e.printStackTrace();
                }
            }
        });

        btnSetTxPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Setando tx power");
                EditText etTxPower = (EditText) findViewById(R.id.etTxPower);
                String power = etTxPower.getText().toString();

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    JSONObject jo = new JSONObject();
                    jo.put("power", power);
                    JSONArray ja = new JSONArray();
                    ja.put(jo);

                    blueBirdController.execute("setTxPower", ja, getApplicationContext());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Power to set = " + power);
            }
        });

        btnGetTxPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Pegando tx power");

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("getTxPower", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao pegar txPower");
                    e.printStackTrace();
                }
            }
        });

        btnSetSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Setando Session");
                EditText etSession = (EditText) findViewById(R.id.etSession);

                String session = etSession.getText().toString();

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    JSONObject jo = new JSONObject();
                    jo.put("session", session);
                    JSONArray ja = new JSONArray();
                    ja.put(jo);

                    blueBirdController.execute("setSession", ja, getApplicationContext());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Sessions to set = " + session);
            }
        });

        btnGetSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Pegando Session");
                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("getSession", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao pegar txSession");
                    e.printStackTrace();
                }
            }
        });

        btnSetTxCicleOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Setando Tx Cycle On");
                EditText etTxCicleOn = (EditText) findViewById(R.id.etTxCicleOn);

                String txCicleOn = etTxCicleOn.getText().toString();

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    JSONObject jo = new JSONObject();
                    jo.put("txCycleOn", txCicleOn);
                    JSONArray ja = new JSONArray();
                    ja.put(jo);

                    blueBirdController.execute("setTxCycleOn", ja, getApplicationContext());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Cicle On to set = " + txCicleOn);
            }
        });

        btnGetTxCicleOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Pegando tx Cycle On");

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("getTxCycleOn", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao pegar TxCycleOn");
                    e.printStackTrace();
                }
            }
        });

        btnSetTxCicleOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Setando tx Cycle Off");
                EditText etTxCicleOff = (EditText) findViewById(R.id.etTxCicleOff);

                String txCicleOff = etTxCicleOff.getText().toString();

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    JSONObject jo = new JSONObject();
                    jo.put("txCycleOff", txCicleOff);
                    JSONArray ja = new JSONArray();
                    ja.put(jo);

                    blueBirdController.execute("setTxCycleOff", ja, getApplicationContext());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Cicle Off to set = " + txCicleOff);
            }
        });

        btnGetTxCicleOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Pegando tx Cycle Off");

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("getTxCycleOff", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao pegar TxCycleOff");
                    e.printStackTrace();
                }
            }
        });

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Load Connection");
                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("loadconnection", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao carregar conexão");
                    e.printStackTrace();
                }
            }
        });


        btnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Status");
                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("statusConnection", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao se conectar");
                    e.printStackTrace();
                }
            }
        });

        btnConectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Conectando");
                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("connect", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao se conectar");
                    e.printStackTrace();
                }
            }
        });

        btnDesconectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Desconectando");
                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("disconnect", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao se disconnect");
                    e.printStackTrace();
                }
            }
        });

        btnIniciarLeitura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Iniciar Leitura");
                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("readTags", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao ler tags");
                    e.printStackTrace();
                }
            }
        });

        btnPararLeitura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Parar Leitura");

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("stop", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao parar leitura");
                    e.printStackTrace();
                }
            }
        });

        btnSetParamInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Settando parametros inventário");
                EditText etSession = (EditText) findViewById(R.id.etSession);
                EditText etTarget = (EditText) findViewById(R.id.etTarget);

                String session = etSession.getText().toString();
                String target = etTarget.getText().toString();

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    JSONArray ja = new JSONArray();
                    JSONObject jo = new JSONObject();
                    jo.put("session", session);
                    jo.put("target", target);
                    ja.put(jo);

                    blueBirdController.execute("setParamInventory", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao carregar conexão");
                    e.printStackTrace();
                }
            }
        });

        btnGetParamInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Settando parametros inventário");
                EditText etSession = (EditText) findViewById(R.id.etSession);
                EditText etTarget = (EditText) findViewById(R.id.etTarget);

                String session = etSession.getText().toString();
                String target = etTarget.getText().toString();

                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    JSONArray ja = new JSONArray();
                    JSONObject jo = new JSONObject();
                    jo.put("session", session);
                    jo.put("target", target);
                    ja.put(jo);

                    blueBirdController.execute("setParamInventory", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao carregar conexão");
                    e.printStackTrace();
                }
            }
        });

        btnGetParamInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("pegando parametros inventário");
                BlueBirdController blueBirdController = new BlueBirdController();

                try {
                    blueBirdController.execute("getParamInventory", null, getApplicationContext());
                } catch (JSONException e) {
                    System.out.println("erro ao pegar parametros");
                    e.printStackTrace();
                }
            }
        });

    }
}
