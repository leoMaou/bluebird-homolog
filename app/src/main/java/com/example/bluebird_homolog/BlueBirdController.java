package com.example.bluebird_homolog;


import android.content.Context;
import android.os.Handler;
import android.os.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import co.kr.bluebird.sled.Reader;
import co.kr.bluebird.sled.SDConsts;

public class BlueBirdController {


    private boolean loopFlag = false;

    private Reader mReader;
    private CharSequence text;
    private Context context;
    private final ConnectivityHandler mConnectivityHandler = new ConnectivityHandler();
    private final ReadTagsHandler mReadTagsHandler = new ReadTagsHandler();
    private final ParamInventoryHandler mParamInventoryHandler = new ParamInventoryHandler();

    public BlueBirdController() {
    }

    public boolean execute(String action, JSONArray args, Context context) throws JSONException {

        this.context = context;

        if (action.equals("loadconnection")) {
            mReader = Reader.getReader(this.context, mConnectivityHandler);
            this.loadConnect();
        }

        if (action.equals("connect")) {
            mReader = Reader.getReader(this.context, mConnectivityHandler);
            this.connect();
        }

        if (action.equals("statusConnection")) {
            mReader = Reader.getReader(this.context, mConnectivityHandler);
            this.getStatusConnection();
        }

        if (action.equals("disconnect")) {
            mReader = Reader.getReader(this.context, mConnectivityHandler);
            this.disconnect();
        }

        if (action.equals("readTags")) {
            mReader = Reader.getReader(this.context, mReadTagsHandler);
            this.readTags();
            return true;
        }

        if (action.equals("stop")) {
            mReader = Reader.getReader(this.context, mReadTagsHandler);
            this.stop();
        }

        if (action.equals("getTarget")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.getTarget();
        }

        if (action.equals("setTarget")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            JSONObject jsonObject = args.getJSONObject(0);
            this.setToggle(SDConsts.RFToggle.OFF);
            this.setTarget(jsonObject.getInt("target"));
        }

        if (action.equals("getTxPower")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.getTxPower();
        }

        if (action.equals("setTxPower")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            JSONObject jsonObject = args.getJSONObject(0);
            this.setTxPower(jsonObject.getInt("power"));
        }

        if (action.equals("setSession")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            JSONObject jsonObject = args.getJSONObject(0);
            this.setSession(jsonObject.getInt("session"));
        }

        if (action.equals("getSession")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.getSession();
        }

        if (action.equals("setTxCycleOn")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            JSONObject jsonObject = args.getJSONObject(0);
            this.setTxCycleOn(jsonObject.getInt("txCycleOn"));
        }

        if (action.equals("getTxCycleOn")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.getTxCycleOn();
        }

        if (action.equals("setTxCycleOff")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            JSONObject jsonObject = args.getJSONObject(0);
            this.setTxCycleOff(jsonObject.getInt("txCycleOff"));
        }

        if (action.equals("getTxCycleOff")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.getTxCycleOff();
        }

        if (action.equals("setToggle")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            JSONObject jsonObject = args.getJSONObject(0);
            this.setToggle(jsonObject.getInt("toggle"));
        }

        if (action.equals("getToggle")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.getToggle();
        }

        if (action.equals("setParamInventory")) {
            JSONObject jsonObject = args.getJSONObject(0);
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.setParamInventory(jsonObject.getInt("session"), jsonObject.getInt("target"));
        }

        if (action.equals("getParamInventory")) {
            mReader = Reader.getReader(this.context, mParamInventoryHandler);
            this.getParamInventory();
        }

        if (action.equals("getBatteryLevel")) {
            mReader = Reader.getReader(this.context, mConnectivityHandler);
            this.getBatteryLevel();
        }

        return false;
    }

    /**
     * Wakes-up the SLED
     * (In case of this API, Run time during about 800 millisecond is required. After the operation completed, it sends related callback message(SLED_WAKEUP(47)))
     */
    private void loadConnect() {
        int ret = -100;
        boolean openResult = false;

        if (mReader != null)
            openResult = mReader.SD_Open();
        if (openResult == SDConsts.SD_OPEN_SUCCESS) {
            System.out.println("Leitor Carregado");
            mReader = Reader.getReader(this.context, mConnectivityHandler);

            ret = mReader.SD_Wakeup();
            if (ret == SDConsts.SDResult.SUCCESS) {
                System.out.println("Leitor ligado");
            } else {
                System.out.println("Leitor não está ligado");
            }

        } else if (openResult == SDConsts.SD_OPEN_FAIL) {
            System.out.println("Não foi possivel abrir o reader");
        }
    }

    /**
     * Disconnects to SLED(SLED DEV STOP)
     */
    private void disconnect() {
        int ret = mReader.SD_Disconnect();
        if (ret == SDConsts.SDConnectState.DISCONNECTED || ret == SDConsts.SDConnectState.ALREADY_DISCONNECTED ||
                ret == SDConsts.SDConnectState.ACCESS_TIMEOUT) {
            System.out.println("Disconnected");
        }
    }

    /**
     * Connects to SLED(SLED DEV START)
     * Operate it after receiving wake-up callback message(SLED_WAKEUP(47))
     */
    private void connect() {
        int ret = -100;
        if (mReader != null) {
            ret = mReader.SD_GetConnectState();
            if (ret == SDConsts.SDConnectState.DISCONNECTED) {
                System.out.println("Connecting");
                ret = mReader.SD_Connect();
            }

            getStatusConnection();
        }
    }

    /**
     * Gets the connection state with the SLED
     */
    private void getStatusConnection() {
        int ret = -100;
        ret = mReader.SD_GetConnectState();
        if (ret == SDConsts.SDConnectState.CONNECTED) {
            System.out.println("Connected");
        } else if (ret == SDConsts.SDConnectState.DISCONNECTED) {
            System.out.println("Disconnected");
        } else {
            System.out.println("Unknown");
        }
    }

    /**
     * Sets the power state value of the RFID radio module
     */
    private void readTags() {

        int ret = -100;
        boolean mIsTurbo = true;
        boolean mMask = false;
        boolean mIgnorePC = false;

        ret = mReader.RF_PerformInventoryWithLocating(mIsTurbo, mMask, mIgnorePC);

        if (ret == SDConsts.RFResult.SUCCESS) {
            System.out.println("Inventory started");
        } else if (ret == SDConsts.RFResult.MODE_ERROR) {
            System.out.println("Start Inventory failed, Please check RFR900 MODE");
        } else if (ret == SDConsts.RFResult.LOW_BATTERY) {
            System.out.println("Start Inventory failed, LOW_BATTERY");
        } else {
            System.out.println("Start Inventory failed");
        }

    }

    /**
     * Stops the inventory operation
     */
    private void stop() {

        int ret = -100;
        ret = mReader.RF_StopInventory();

        if (ret == SDConsts.RFResult.SUCCESS || ret == SDConsts.RFResult.NOT_INVENTORY_STATE) {

            System.out.println("Inventory stopped with success");
        } else if (ret == SDConsts.RFResult.STOP_FAILED_TRY_AGAIN) {
            System.out.println("Stop Inventory failed");
        }

    }

    /**
     * Gets inventory session target of RFID radio module
     */
    private void getTarget() {

        if (mReader != null) {
            int sessionTarget = mReader.RF_GetInventorySessionTarget();

            if (sessionTarget == SDConsts.RFInvSessionTarget.TARGET_A) {
                System.out.println("Get target: Target A");
            } else if (sessionTarget == SDConsts.RFInvSessionTarget.TARGET_B) {
                System.out.println("Get target: Target B");
            } else if (sessionTarget == SDConsts.RFInvSessionTarget.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Get target: Cannot get target");
            } else if (sessionTarget == SDConsts.RFInvSessionTarget.SD_NOT_CONNECTED) {
                System.out.println("Get target: Reader not connected");
            } else {
                System.out.println("Get target: Unknown Error");
            }
        }
    }

    /**
     * Sets the inventory session target of the RFID radio module Only operate when the toggle state is OFF
     * (Emerson) Imagino que togle seja uma função do bluebird para setar a leitura A_B
     */
    private void setTarget(int target) {

        if (mReader != null) {
            int ret = mReader.RF_SetInventorySessionTarget(target);

            if (ret == SDConsts.RFResult.SUCCESS) {
                System.out.println("Target Setted");
                getTarget();
            } else if (ret == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Set target: Invalid param");
            } else if (ret == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Set target: Serial Connction Error");
            } else if (ret == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Set target: Reader not connected");
            } else if (ret == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Set target: Reader error, cannot set target");
            } else {
                System.out.println("Set target: Unknown Error");
            }
        }
    }


    /**
     * Gets the power state value of the RFID radio module
     */
    private void getTxPower() {
        if (mReader != null) {
            int powerState = mReader.RF_GetRadioPowerState();

            if (powerState >= SDConsts.RFPower.MIN_POWER && powerState <= SDConsts.RFPower.MAX_POWER) {
                System.out.println("Get power: " + powerState);
            } else if (powerState == SDConsts.RFInvSessionTarget.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Get power: Reader error, Cannot get power");
            } else if (powerState == SDConsts.RFInvSessionTarget.SD_NOT_CONNECTED) {
                System.out.println("Get power: Reader not connected");
            } else {
                System.out.println("Get target: Unknown Error");
            }
        }
    }

    /**
     * Sets the power state value of the RFID radio module
     *
     * @param power - RFPower - The power level for the antenna port(5 ~ 30) : 30(default)
     */
    private void setTxPower(int power) {

        if (mReader != null) {
            int ret = mReader.RF_SetRadioPowerState(power);

            if (ret == SDConsts.RFResult.SUCCESS) {
                System.out.println("power Setted");
                getTxPower();
            } else if (ret == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Set power: Invalid param");
            } else if (ret == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Set power: Serial Connction Error");
            } else if (ret == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Set power: Reader not connected");
            } else if (ret == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Set power: Reader error, cannot set power");
            } else {
                System.out.println("Set power: Unknown Error");
            }
        }
    }

    /**
     * Sets the session value of the RFID radio module(Session flag will be matched against the inventory state specified by target)
     * Only operate when the toggle state is OFF
     *
     * @param session -Value of the Session(0 ~ 3) : 0(default)
     */
    private void setSession(int session) {

        if (mReader != null) {
            int ret = mReader.RF_SetSession(session);

            if (ret == SDConsts.RFResult.SUCCESS) {
                System.out.println("session Setted");
                getSession();
            } else if (ret == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Set session: Invalid param");
            } else if (ret == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Set session: Serial Connction Error");
            } else if (ret == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Set session: Reader not connected");
            } else if (ret == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Set session: Reader error, cannot set session");
            } else {
                System.out.println("Set session: Unknown Error");
            }
        }
    }

    /**
     * Gets the session value of the RFID radio module(Session flag will be matched against the inventory state specified by target)
     */
    private void getSession() {

        if (mReader != null) {
            int sessionState = mReader.RF_GetSession();

            if (sessionState >= SDConsts.RFSession.SESSION_S0 && sessionState <= SDConsts.RFSession.SESSION_S3) {
                System.out.println("Get session" + sessionState);
            } else if (sessionState == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Get session: Invalid param");
            } else if (sessionState == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Get session: Serial Connction Error");
            } else if (sessionState == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Get session: Reader not connected");
            } else if (sessionState == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Get session: Reader error, cannot get session");
            } else {
                System.out.println("Get session: Unknown Error");
            }
        }
    }

    /**
     * Sets the dwell time of RFID radio module
     *
     * @param txCycleOn - The number of milliseconds to spend on this antenna port during a cycle (50 ~ 400) : 200(default) - (WorkTime) working time
     */
    private void setTxCycleOn(int txCycleOn) {
        if (mReader != null) {
            int ret = mReader.RF_SetDwelltime(txCycleOn);

            if (ret == SDConsts.RFResult.SUCCESS) {
                System.out.println("tx cycle on Setted");
                getTxCycleOn();
            } else if (ret == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Set tx cycle on: Invalid param");
            } else if (ret == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Set tx cycle on: Serial Connction Error");
            } else if (ret == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Set tx cycle on: Reader not connected");
            } else if (ret == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Set tx cycle on: Reader error, cannot set tx cycle on");
            } else {
                System.out.println("Set tx cycle on: Unknown Error");
            }
        }
    }

    /**
     * Gets the dwell time (50 ~ 400, 200(default)) of the RFID radio module
     */
    private void getTxCycleOn() {
        if (mReader != null) {
            int dwellTimeState = mReader.RF_GetDwelltime();

            if (dwellTimeState >= SDConsts.RFDwell.MIN_DWELL && dwellTimeState <= SDConsts.RFDwell.MAX_DWELL) {
                System.out.println("Get tx cycle on" + dwellTimeState);
            } else if (dwellTimeState == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Get tx cycle on: Invalid param");
            } else if (dwellTimeState == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Get tx cycle on: Serial Connction Error");
            } else if (dwellTimeState == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Get tx cycle on: Reader not connected");
            } else if (dwellTimeState == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Get tx cycle on: Reader error, cannot get tx cycle on");
            } else {
                System.out.println("Get tx cycle on: Unknown Error");
            }
        }
    }


    /**
     * Sets the duty cycle value of the RFID radio module
     *
     * @param txCycleOff - millisec - Import or set the resting time(millisecond) of each ports(0 ~ 1000) : 100(default)
     */
    private void setTxCycleOff(int txCycleOff) {
        if (mReader != null) {
            int ret = mReader.RF_SetDutyCycle(txCycleOff);

            if (ret == SDConsts.RFResult.SUCCESS) {
                System.out.println("tx cycle off Setted");
                getTxCycleOff();
            } else if (ret == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Set tx cycle off: Invalid param");
            } else if (ret == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Set tx cycle off: Serial Connction Error");
            } else if (ret == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Set tx cycle off: Reader not connected");
            } else if (ret == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Set tx cycle off: Reader error, cannot set tx cycle off");
            } else {
                System.out.println("Set tx cycle off: Unknown Error");
            }
        }
    }

    /**
     * Gets the duty cycle value of the RFID radio module
     */
    private void getTxCycleOff() {
        if (mReader != null) {
            int dutyState = mReader.RF_GetDutyCycle();

            if (dutyState >= SDConsts.RFDutyCycle.MIN_DUTY && dutyState <= SDConsts.RFDutyCycle.MAX_DUTY) {
                System.out.println("Get tx cycle off " + dutyState);
            } else if (dutyState == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Get tx cycle off: Invalid param");
            } else if (dutyState == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Get tx cycle off: Serial Connction Error");
            } else if (dutyState == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Get tx cycle off: Reader not connected");
            } else if (dutyState == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Get tx cycle off: Reader error, cannot get tx cycle off");
            } else {
                System.out.println("Get tx cycle off: Unknown Error");
            }
        }
    }

    /**
     * Sets the duty cycle value of the RFID radio module
     *
     * @param RFToggle - Sets the toggle state of the RFID radio module
     *                 (A flag that indicates, after performing the inventory cycle for the specified target, if the target should be toggled and another inventory cycle run)
     */
    private void setToggle(int RFToggle) {
        if (mReader != null) {
            int ret = mReader.RF_SetToggle(RFToggle);

            if (ret == SDConsts.RFResult.SUCCESS) {
                System.out.println("toggle Setted");
                getToggle();
            } else if (ret == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Set toggle: Invalid param");
            } else if (ret == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Set toggle: Serial Connction Error");
            } else if (ret == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Set toggle: Reader not connected");
            } else if (ret == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Set toggle: Reader error, cannot set toggle");
            } else {
                System.out.println("Set toggle: Unknown Error");
            }
        }
    }

    /**
     * Gets the toggle state of the RFID radio module
     * (A flag that indicates, after performing the inventory cycle for the specified target, if the target should be toggled and another inventory cycle run)
     */
    private void getToggle() {
        if (mReader != null) {
            int toggleState = mReader.RF_GetToggle();

            if (toggleState == SDConsts.RFToggle.ON) {
                System.out.println("Get toggleState: ON " + toggleState);
            } else if (toggleState == SDConsts.RFToggle.OFF) {
                System.out.println("Get toggleState: OFF " + toggleState);
            } else if (toggleState == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Get toggleState: Invalid param");
            } else if (toggleState == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Get toggleState: Serial Connction Error");
            } else if (toggleState == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Get toggleState: Reader not connected");
            } else if (toggleState == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Get toggleState: Reader error, cannot get toggleState");
            } else {
                System.out.println("Get toggleState: Unknown Error");
            }
        }
    }

    /**
     * Gets the battery status(value) of the SLED
     */
    private void getBatteryLevel() {
        if (mReader != null) {
            int batteryState = mReader.SD_GetBatteryStatus();

            if (batteryState >= SDConsts.SDBatteryState.MIN && batteryState <= SDConsts.SDBatteryState.MAX) {
                System.out.println("Get battery" + batteryState);
            } else if (batteryState == SDConsts.RFResult.ARGUMENT_ERROR) {
                System.out.println("Get battery: Invalid param");
            } else if (batteryState == SDConsts.RFResult.OTHER_CMD_RUNNING_ERROR) {
                System.out.println("Get battery: Serial Connction Error");
            } else if (batteryState == SDConsts.RFResult.SD_NOT_CONNECTED) {
                System.out.println("Get battery: Reader not connected");
            } else if (batteryState == SDConsts.RFResult.READER_OR_COM_INTERFACE_STATUS_ERROR) {
                System.out.println("Get battery: Reader error, cannot get battery");
            } else {
                System.out.println("Get battery: Unknown Error");
            }
        }
    }

    private void setParamInventory(int session, int target) {

        if (mReader != null) {
            this.setToggle(SDConsts.RFToggle.OFF);
            this.setSession(session);
            this.setTarget(target);
        }
    }

    private void getParamInventory() {

        if (mReader != null) {
            this.getSession();
            this.getTarget();
        }
    }

    private static class ConnectivityHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            System.out.println("Connect Message Handler:" + msg);
        }
    }

    private static class ParamInventoryHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            System.out.println("Params Message Handler:" + msg);
        }
    }

    private static class ReadTagsHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            String data = msg.toString();
            if (data.contains(";")) {
                System.out.println("Full info tag: " + data);
            }
        }
    }

}
